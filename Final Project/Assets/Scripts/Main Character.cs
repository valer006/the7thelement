﻿using UnityEngine;
using System.Collections;

public class MainCharacter : MonoBehaviour {

    private int characterHealth;

	// Use this for initialization
	void Start () {
        characterHealth = 100;
	}
	
	// Update is called once per frame
	void Update () {
        if (characterHealth <= 0)
            Application.LoadLevel("Game Over");
	}
}
