﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
	public GameObject green;
	public GameObject green1;
	public GameObject yellow;
	public GameObject yellow1;
	public GameObject yellow2;
	public GameObject orange;
	public GameObject orange1;
	public GameObject orange2;
	public GameObject red;
	public GameObject red1;
	public int health;
	int damage = 5;
	int healthPackSmall = 20;
	int healthPackLarge = 100;
	public Text healthText;

	public Image damageImage;
	public float flashSpeed = 5f;                           
    public Color flashColour = new Color(1f, 1f, 1f, 0.1f);

    Animator anim;                                          
    PlayerController playerController;                      
    bool isDead;                                            
    bool damaged;                                           


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerController = GetComponent <PlayerController> ();
    }

	void Start () {

		green.SetActive (true);
		green1.SetActive (true);
		yellow.SetActive (true);
		yellow1.SetActive (true);
		yellow2.SetActive (true);
		orange.SetActive (true);
		orange1.SetActive (true);
		orange2.SetActive (true);
		red.SetActive (true);
		red1.SetActive (true);
		setHealthText ();
	}

	void Update () {
		if (damaged) {
            damageImage.color = flashColour;
        }
        else {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;

		CheckHealth ();//checks health for health bar update
		setHealthText();
	}

	public void TakeDamage (int amount)
    {
        damaged = true;
        health -= amount;
    }

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Enemy")
			health -= damage;
		if (other.gameObject.tag == "SmallHP") {
            if (health < 100)
            {
                health += healthPackSmall;
                other.gameObject.SetActive(false);
            }
        }
		if (other.gameObject.tag == "LargeHP")
			health += healthPackLarge;
		if (health > 100)
			health = 100;
		if (health <= 0)
			health = 0;	
	}

	void CheckHealth(){
		if (health >= 100) {
			green.SetActive (true);
			green1.SetActive (true);
			yellow.SetActive (true);
			yellow1.SetActive (true);
			yellow2.SetActive (true);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 90) {
			green.SetActive (false);
			green1.SetActive (true);
			yellow.SetActive (true);
			yellow1.SetActive (true);
			yellow2.SetActive (true);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 80) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (true);
			yellow1.SetActive (true);
			yellow2.SetActive (true);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 70) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (true);
			yellow2.SetActive (true);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 60) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (true);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 50) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (true);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 40) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (false);
			orange1.SetActive (true);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 30) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (false);
			orange1.SetActive (false);
			orange2.SetActive (true);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 20) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (false);
			orange1.SetActive (false);
			orange2.SetActive (false);
			red.SetActive (true);
			red1.SetActive (true);
		} else if (health >= 10) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (false);
			orange1.SetActive (false);
			orange2.SetActive (false);
			red.SetActive (false);
			red1.SetActive (true);
		} else if (health >= 0) {
			green.SetActive (false);
			green1.SetActive (false);
			yellow.SetActive (false);
			yellow1.SetActive (false);
			yellow2.SetActive (false);
			orange.SetActive (false);
			orange1.SetActive (false);
			orange2.SetActive (false);
			red.SetActive (false);
			red1.SetActive (false);
            Application.LoadLevel("Game Over");
		}
	}

	void setHealthText()
	{
		healthText.text = "Health " + health.ToString() + "%";
	}

}
