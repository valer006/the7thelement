﻿using UnityEngine;
using System.Collections;

public class StartOver : MonoBehaviour
{
    public GameObject myPrefab;

    public void onClickButton1()
    {
        Application.Quit();
    }

    public void onClickButton2()
    {
        Application.LoadLevel("Level 1");
    }
}
