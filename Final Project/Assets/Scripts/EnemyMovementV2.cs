﻿using UnityEngine;
using System.Collections;

public class EnemyMovementV2 : MonoBehaviour {
   
    Transform player;
    HealthBar playerHealth;
    EnemyHealth enemyHealth;     
    NavMeshAgent nav; 

	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <HealthBar> ();
        
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <NavMeshAgent> ();
	}

	void Update ()
    {
        if(enemyHealth.currentHealth > 0 && playerHealth.health > 0)
        {
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    } 
}
