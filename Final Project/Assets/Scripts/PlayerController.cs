﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

   	public float speed = 8.0F;

   	void Start () {
   		Cursor.lockState = CursorLockMode.Locked;
   	}

   	void Update () {
   		float moveVertical = Input.GetAxis("Vertical") * speed;
   		float moveHorizontal = Input.GetAxis("Horizontal") * speed;

   		moveVertical *= Time.deltaTime;
   		moveHorizontal *= Time.deltaTime;

   		transform.Translate(moveHorizontal, 0, moveVertical);

   		if (Input.GetKeyDown("escape")) {
   			Cursor.lockState = CursorLockMode.None;
   		}
   	}
}