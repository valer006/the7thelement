﻿using UnityEngine;
using System.Collections;

public class MenuButtonPress : MonoBehaviour
{

    public GameObject myPrefab;
    public void onClickButton1()
    {
        Application.LoadLevel("Level 1");
    }

    public void onClickButton2()
    {
        Application.Quit();
    }

    public void onClickButton3()
    {
        Application.LoadLevel("Main Menu 2");
    }
}
