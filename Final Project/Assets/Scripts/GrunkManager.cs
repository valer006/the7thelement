﻿using UnityEngine;

public class GrunkManager : MonoBehaviour
{
    public HealthBar playerHealth;       // Reference to the player's heatlh.
    public GameObject enemy;                // The enemy prefab to be spawned.
    public float cloningTime = 3f;            // How long between each spawn.
    public Transform[] cloningPoints;         // An array of the spawn points this enemy can spawn from.

    public int maxNumberOfEnemies = 10;

    void Start ()
    {
    	InvokeRepeating ("Clone", cloningTime, cloningTime);
    }


    void Clone ()
    {	
        if(playerHealth.health <= 0f || maxNumberOfEnemies == 0)
        {
            return;
        }

        int cloningPointIndex = Random.Range (0, cloningPoints.Length);

        Instantiate (enemy, cloningPoints[cloningPointIndex].position, cloningPoints[cloningPointIndex].rotation);

        maxNumberOfEnemies -= 1;
    }
}