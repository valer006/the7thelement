﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    private float forwardMovement;

	void Start () {
        forwardMovement = 25f;
	}
	
	void Update () {
        if (forwardMovement < 255.10f)
        {
            Vector3 move = new Vector3(441.0f, 2.0f, forwardMovement);
            transform.position = move;
            forwardMovement = forwardMovement + .15f;
        }
    }
}
