﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 99;  
    public int currentHealth;        
    public float sinkSpeed = 2.5f;   
    public int scoreValue = 10;      

    ParticleSystem hitParticles;     
    CapsuleCollider capsuleCollider; 
    bool isDead;                    
    bool updateScore;

    public int totalWinningPoints = 50;

    void Awake ()
    {
	    hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        currentHealth = startingHealth;

        updateScore = false;
    }

    void Update ()
    {
        if(currentHealth <= 0 && !updateScore) {
            Death ();
        }

        Debug.Log("score: " + ScoreManager.score);
        Debug.Log("totalWinningPoints: " + totalWinningPoints);

        if (ScoreManager.score == totalWinningPoints) {
            Debug.Log("win");
            Application.LoadLevel("Win");
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        
        if(isDead) {
            return;            
        }

        currentHealth -= amount;
            
        Debug.Log ("Current Enemy Health: " + currentHealth);
    }


    void Death ()
    {   
        Debug.Log ("Enemy is dead");

        isDead = true;

        GetComponent <NavMeshAgent> ().enabled = false;

        GetComponent <Rigidbody> ().isKinematic = true;

        capsuleCollider.isTrigger = true;

        Destroy (gameObject, 0.5f);

        ScoreManager.score += scoreValue;

        updateScore = true;

        transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
    }
}